﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silnia
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Podaj liczbę do wyliczenia silni (max 29): ");
            //zwykla silnia
            //      decimal d = 0;
            //    Decimal.TryParse(Console.ReadLine(), out d);
            //    Console.WriteLine("Silnia to " + silnia(d).ToString());
            //silnia v2
            int d = 0;
            Int32.TryParse(Console.ReadLine(), out d);
             string[] sil = new string[d];
            silniav2(d-1,sil);
            Console.WriteLine("Silnia to " + sil[d-1]);
            Console.ReadKey();

        }
        static void silniav2(int c, string[]s)
        {
            
            if (c < 1)
            {
                s[0] = "1";
                return;
            }
            else
            {
                silniav2(c - 1, s);
            }
            if (c < 27)
            {
                decimal t;
                Decimal.TryParse(s[c - 1], out t);
                s[c] = (t * (c + 1)).ToString();
            }
            else
            //podzielenie stringa na dwie części (bo jest poza zakresem decimala) i liczenie dwóch części a następnie ich połączenie
            {
                //podzielenie poprzedniej silni na dwie części
                decimal l, r;
                int p = s[c - 1].Length / 2;
                string ls = s[c-1].Substring(0, p );
                string rs = s[c-1].Substring(p, p+1 );
                Decimal.TryParse(ls, out l);
                Decimal.TryParse(rs, out r);
                l *= c + 1;
                r *= c + 1;
                //przygotowanie prawej części do złączenia z lewą
                string ts;
                int lengthDiff;
                ts =r.ToString();
                lengthDiff =ts.Length-rs.Length;
                string sToAddToLs = ts.Substring(0,lengthDiff);
                string sToStayOnRight = ts.Substring(lengthDiff, rs.Length);
                decimal toAdd;
                Decimal.TryParse(sToAddToLs, out toAdd);
                l += toAdd;
                s[c] = l.ToString() + sToStayOnRight.ToString();
            }
        }
        static decimal silnia(decimal s)
        {
            decimal n = s - 1;
            if (n > 1)
                s *= silnia(n);
            return s;
        }
    }

    
}
